#include <mpi.h>
#include <iostream>
#include<Windows.h>
#include <time.h>

double dotProduct(double* a, double* b, int n) {
    double res = 0.0;
    for (int i = 0; i < n; i++) {
        res += a[i] * b[i];
    }
    return res;
}

int main(int argc, char* argv[]) {
    double result = 0;
    int rank;
    int num_procs;
    int length = atoi(argv[1]);
    srand((unsigned)time(NULL));

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0)
        printf("Using %d tasks\n", num_procs);


    int rem = length % num_procs;
    int real_length = length;
    if (rem != 0)
        real_length = (length / num_procs + 1) * num_procs;
    int length2 = real_length / num_procs;
    double* x_arr = new double[real_length];
    double* y_arr = new double[real_length];

    // вектора, которые получают процессы
    double* x_part = new double[length2];
    double* y_part = new double[length2];

    if (rank == 0) {
        for (int i = 0; i < real_length; i++) {
            if (i < length) {
                x_arr[i] = rand() % 2;
                x_arr[i] -= 1;
                y_arr[i] = rand() % 2;
                y_arr[i] -= 1;
            }
            else {
                x_arr[i] = 0;
                y_arr[i] = 0;
            }
        }

        std::cout << "Vector 1" << std::endl;
        for (int i = 0; i < length; i++)
        {
            std::cout << x_arr[i] << " ";
        }
        std::cout << "\n";
        std::cout << "Vector 2" << std::endl;
        for (int i = 0; i < length; i++)
        {
            std::cout << y_arr[i] << " ";
        }
        std::cout << "\n";
    }

    MPI_Scatter(x_arr, length2, MPI_DOUBLE, x_part, length2, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(y_arr, length2, MPI_DOUBLE, y_part, length2, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    double temp = dotProduct(x_part, y_part, length2);

    MPI_Reduce(&temp, &result, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    if (rank == 0) {
        std::cout << "Result: " << result;
    }
    delete[] x_arr;
    delete[] y_arr;
    delete[] x_part;
    delete[] y_part;
    MPI_Finalize();

    return 0;
}

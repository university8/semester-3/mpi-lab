#include <mpi.h>
#include <limits>
#include <iostream>
#include <time.h>

void printMatrix(double* a, int row, int col) {
    for (int i = 0; i < row; i++)
    {
        for (int j = 0; j < col; j++) {
            std::cout << *(a + i * col + j) << " ";
        }
        std::cout << std::endl;
    }
}
int main(int argc, char* argv[]) {
    int rank;
    int num_procs;
    int length = 0;
    int cols = 0;
    srand((unsigned)time(NULL));

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        printf("Using %d tasks\n", num_procs);
        std::cout << "Rows and colums:" << std::endl;
        
        std::cin >> length;
        std::cin >> cols;
    }


    int rem = length % num_procs;
    int real_length = length;
    if (rem != 0)
        real_length = (length / num_procs + 1) * num_procs;
    int length2 = real_length / num_procs;

    double* matrix1 = new double[real_length * cols];
    double* matrix2 = new double[real_length * cols];
    double* res_matrix = new double[real_length * cols];

    double* m1_part = new double[length2 * cols];
    double* m2_part = new double[length2 * cols];
    double* res_part = new double[length2 * cols];

    if (rank == 0) {
        for (int i = 0; i < real_length; i++) {
            for (int j = 0; j < cols; j++)
            {
                if (i < length) {
                    *(matrix1 + i * cols + j) = (rand() % 3) - 1;
                    *(matrix2 + i * cols + j) = (rand() % 3) - 1;
                }
                else {
                    *(matrix1 + i * cols + j) = 0;
                    *(matrix2 + i * cols + j) = 0;
                }
            }
        }

        std::cout << "Matrix 1" << std::endl;
        printMatrix(matrix1, length, cols);
        std::cout << "\n";
        std::cout << "Matrix 2" << std::endl;
        printMatrix(matrix2, length, cols);
        std::cout << "\n";
    }

    MPI_Scatter(matrix1, length2 * cols, MPI_DOUBLE, m1_part, length2 * cols, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(matrix2, length2 * cols, MPI_DOUBLE, m2_part, length2 * cols, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    for (int i = 0; i < length2 * cols; i++) {
        res_part[i] = m1_part[i] - m2_part[i];
    }

    MPI_Gather(res_part, length2 * cols, MPI_DOUBLE,
        res_matrix, length2 * cols, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        std::cout << "Result " << std::endl;
        printMatrix(res_matrix, length, cols);
    }

    delete[] matrix1;
    delete[] matrix2;
    delete[] m1_part;
    delete[] m2_part;
    delete[] res_part;
    delete[] res_matrix;
    MPI_Finalize();

    return 0;
}
